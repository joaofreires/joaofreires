---
title: "Overcoming Caching Scaling Hurdles"
date: "2022-12-02"
description: ""
tags: ["caching", "scaling", "distributed-systems"]
ShowToc: false
ShowBreadCrumbs: false
---
Recently, I had an opportunity to guide one of my interns in scaling a service, incorporating a load balancer and additional instances. As we navigated this process, it became an educational journey for both of us, where we had to anticipate and counter potential complications. While I cannot claim to have a complete grasp of all potential issues, certain challenges recur so frequently in scaling that they've become nearly impossible to overlook.

For that time, before we added a distributed caching tool, I let it go and showed him the magic.

### Caching Challenges:

Caching, if not meticulously managed, can morph into a double-edged sword, especially in the context of service scaling. When cache is not uniformly distributed across all instances, it paves the way for various problems. For instance, let's consider a scenario where a specific instance invalidates its cache due to an update. If cache invalidation is confined to that particular instance, it leaves the remaining instances with outdated cache.

Conversely, if we use cache to store rate limits for resource access, each instance will operate with its own independent counter. Therefore, if a resource is supposed to accommodate 100 accesses per hour, each instance maintaining its own counter implies that the overall access rate balloons to 100 times the number of instances.

These issues can provoke a multitude of bugs, such as data inconsistency. For instance, each time the load balancer selects an instance, users may encounter outdated results due to uneven cache status.

### Overcoming Caching Dilemmas:

In the face of these caching issues, the utilization of in-memory data storage services such as Redis and memcached offers a compelling solution. They address these problems effectively, thereby explaining their popularity in managing scalable services.

Redis and memcached ensure data consistency by providing a shared, distributed caching system. This means that when an instance invalidates part of its cache, the change is reflected across all instances, effectively eliminating stale data. Similarly, for rate limiting, these services provide a shared counter that keeps track of resource accesses cohesively, preventing any breach of access limits even when multiple instances are involved.

### New Solution, New Problems

Cache invalidation is one of the key challenges in caching. It is the process of deciding which entries must be deleted (or marked as outdated) when a limit is reached or the underlying data changes. In a distributed system, invalidating the cache can be complicated as you need to ensure consistency across all instances.

For instance, if a cache is invalidated due to data changes on one instance, but the others aren't informed, it may result in serving stale data. Cache invalidation strategies should be implemented to update or remove cache entries in all instances once the data changes. It is really important when we are working with microservices that use cached data.

Assuming that the challenge of cache invalidation has been effectively resolved, it might seem like we've finally achieved the ideal functioning state for our system - a state where we can sit back and watch our well-designed system operate seamlessly. Indeed, with the right decisions and certain specific conditions, this might be the case. However, there's a lesser-discussed but significant issue that we need to address: cache thrashing.

Cache thrashing occurs when the working set, or the data that the system frequently uses, exceeds the capacity of the cache. Consequently, the system is forced into a constant cycle of evicting entries from the cache to accommodate new data, resulting in high latency and low hit rates. This essentially means that our cache begins to discard older data.

However, within the realm of caching, older and computationally intensive data that is accessed frequently can often be more valuable than newer data that is used only once. Hence, implementing an effective eviction policy is crucial to prevent cache thrashing.

Eviction policies such as Least Recently Used (LRU) or Least Frequently Used (LFU) are particularly beneficial in this scenario. They help prioritize the preservation of valuable data in the cache, thereby minimizing the negative effects of cache thrashing. Ultimately, these strategies can enhance your system's performance and resource utilization significantly.

To delve deeper into this topic and learn about various eviction policies, the Redis documentation on eviction policies is an excellent resource. It provides a comprehensive overview of the different strategies you can employ to maintain an efficient cache in your system.


### Last Words
However, it's important to remember that scaling isn't just about managing cache. It involves a multitude of factors, including load balancing, database sharding, data consistency, and many more. Ultimately, the key to successfully scaling a service lies in anticipating potential issues and adopting proactive strategies to handle them.
