---
title: "About"
disableShare: true
---


Hey there! 👋

I'm João (Pronounced as: /ʒoão/ or Joh-(**[here a lightsaber swing sound](https://www.youtube.com/watch?v=ICiIIo3aAds)**)), a software engineer who loves to get hands-on with programming languages in general, the ones I worked most are Python, JS, Golang, Rust, Elixir. I've been working on everything from architecture tools to maintaining legacy code and managing data pipelines. I also enjoy tutoring, I was cryptography and database tutor when I was in college and after that I volunteer to be a programming teacher for kids.

In this not so maintained "blog", I'll share my adventures in coding, experiences working in agile environments, tips and tricks I've picked up along the way, and my thoughts on the latest in the tech world. So whether you're a fellow techie or just interested in the field, stick around for some interesting insights. Let's explore this wild tech world together! 🚀
